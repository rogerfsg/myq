<?php
declare(strict_types=1);
namespace RobotMyQTests;

use RobotMyQTests\TestsConstants;

use PHPUnit\Framework\TestCase;

use RobotMyQ\Context;
use RobotMyQ\Robot;
use RobotMyQ\FactoryCommand;
use RobotMyQ\Map;
use RobotMyQ\ItemMap;

class CommandBackTest extends TestCase
{


    /*
     * C    C       C
     * S    Robot-> S
     * C    S       C
     *
     * Try to advance then hit. Turn right and advance. Then not hit
     */
    public function testBackCommandUntilStep1()
    {
        $map = new Map();
        $map->init(new ItemMap(3, 3, Map::STATE_FLOOR_DIRT));

        $robot= new Robot($map);
        $robot->start(new \RobotMyQ\ItemRobot(1,1, Robot::DIRECTION_EAST, 100));

        $f = new FactoryCommand($robot);
        $cmd="B";
        $f2= $f->factory($cmd);
        $state = $f2->execute();
        $this->assertEquals(\RobotMyQ\RobotActionState::RESULT_EXECUTED, $state, "Has to be executed");
        $this->assertEquals("1, 0", (string)$robot->getPosition(), "Position diferent than expected");

    }

}
