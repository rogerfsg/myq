<?php
declare(strict_types=1);
namespace RobotMyQTests;

use RobotMyQTests\TestsConstants;

use PHPUnit\Framework\TestCase;
use RobotMyQ\FactoryCommand;
use RobotMyQ\CommandTurnLeft;
use RobotMyQ\CommandTurnRight;
use RobotMyQ\FactoryContextFromFile;
use RobotMyQ\Context;
use RobotMyQ;
use RobotMyQ\Constants;

class CommandTurnLeftTest extends TestCase
{


    /*
     * Will execute every command provinient form test 1
     */
    public function testTurnLeft()
    {
        $factory = new FactoryContextFromFile(TestsConstants::FILE_TEST_1);
        $context = $factory->factory();
        $robot= $context->getRobot();
        $robot->start(new \RobotMyQ\ItemRobot( 0,0, RobotMyQ\Robot::DIRECTION_NORTH, 100));
        $f = new FactoryCommand($robot);
        $cmd="TL";
        $f2= $f->factory($cmd);
        $f2->execute();
        //assertAttributeEquals($expected, $actualAttributeName, $actualClassOrObject, $message = ''
        $this->assertAttributeEquals(
            RobotMyQ\Robot::DIRECTION_WEST
                , 'orientation'
                , $robot->getItem()
                , "Robot has to fancing west");
    }

}
