<?php
declare(strict_types=1);
namespace RobotMyQTests;

use RobotMyQTests\TestsConstants;

use PHPUnit\Framework\TestCase;
use RobotMyQ\Constants;

class ConstantsTest extends TestCase
{
    public function testeUrl()
    {
        $c = new Constants();
        self::assertNotEmpty(
            $c->URL_API(),
            "Url API its empt. ".$c->URL_API().". Create a .env file and put UrlApp the pointer to the URL api. "
        );
    }
}