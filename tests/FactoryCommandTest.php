<?php
declare(strict_types=1);
namespace RobotMyQTests;

use RobotMyQTests\TestsConstants;

use PHPUnit\Framework\TestCase;
use RobotMyQ\FactoryCommand;
use RobotMyQ\CommandTurnLeft;
use RobotMyQ\CommandTurnRight;
use RobotMyQ\CommandAdvance;
use RobotMyQ\CommandClean;
use RobotMyQ\FactoryContextFromFile;
use RobotMyQ\Context;
use RobotMyQ\Constants;

class FactoryCommandTest extends TestCase
{
    private $context;
    public function setUp()
    {
        $factory = new FactoryContextFromFile(TestsConstants::FILE_TEST_1);
        $this->context = $factory->factory();
    }
    /*
     * Will execute every command provinient form test 1
     */
    public function testFactoryCommandTurnLeft()
    {


        $f = new FactoryCommand($this->context->getRobot());
        $cmd="TL";
        $f2= $f->factory($cmd);
        $this->assertEquals((string) $f2, $cmd, "Expected turn left");
        $this->assertTrue( $f2 instanceof CommandTurnLeft, "Expected object of type command left");
    }

    /*
     * Will execute every command provinient form test 1
     */
    public function testFactoryCommandTurnRigth()
    {
        $factory = new FactoryContextFromFile(TestsConstants::FILE_TEST_1);
        $f = new FactoryCommand($this->context->getRobot());
        $cmd="TR";
        $f2= $f->factory($cmd);
        $this->assertEquals((string) $f2, $cmd, "Expected turn right");
        $this->assertTrue( $f2 instanceof CommandTurnRight, "Expected object of type command right");
    }

    /*
   * Will execute every command provinient form test 1
   */
    public function testFactoryCommandAdvance()
    {
        $factory = new FactoryContextFromFile(TestsConstants::FILE_TEST_1);
        $f = new FactoryCommand($this->context->getRobot());
        $cmd="A";
        $f2= $f->factory($cmd);
        $this->assertEquals((string) $f2, $cmd, "Expected advance");
        $this->assertTrue( $f2 instanceof CommandAdvance, "Expected object of type command right");
    }

    public function testFactoryCommandClean()
    {
        $factory = new FactoryContextFromFile(TestsConstants::FILE_TEST_1);
        $f = new FactoryCommand($this->context->getRobot());
        $cmd="C";
        $f2= $f->factory($cmd);
        $this->assertEquals((string) $f2, $cmd, "Expected advance");
        $this->assertTrue( $f2 instanceof CommandClean, "Expected object of type command clean");
    }
}
