<?php
declare(strict_types=1);
namespace RobotMyQTests;

/**
 * Constants containing tests input and constants
 *
 */
class TestsConstants
{

    const RSC = __DIR__."/../rsc/";
    /**
     * Input file 1 of the example
     */
    const FILE_TEST_1=TestsConstants::RSC."test1.json";

    /**
     * Input file 2 of the example
     */
    const FILE_TEST_2=TestsConstants::RSC."test2.json";

    /**
     * File Result 1 of the example
     */
    const FILE_RESULT_TEST_1=TestsConstants::RSC."test1_result.json";

    /**
     * File Result 2 of the example
     */
    const FILE_RESULT_TEST_2=TestsConstants::RSC."test2_result.json";

}
