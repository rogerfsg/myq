<?php
declare(strict_types=1);
namespace RobotMyQTests;

use RobotMyQTests\TestsConstants;

use PHPUnit\Framework\TestCase;
use RobotMyQ\FactoryCommand;
use RobotMyQ\CommandTurnLeft;
use RobotMyQ\CommandTurnRight;
use RobotMyQ\Context;
use RobotMyQ;
use RobotMyQ\FactoryContextFromFile;
use RobotMyQ\Constants;

class CommandTurnRightTest extends TestCase
{
    /*
     * Will execute every command provinient form test 1
     */
    public function testTurnRight()
    {
        $factory = new FactoryContextFromFile(TestsConstants::FILE_TEST_1);
        $context = $factory->factory();
        $robot= $context->getRobot();
        $robot->start(new \RobotMyQ\ItemRobot(0,0, RobotMyQ\Robot::DIRECTION_NORTH, 100));
        $f = new FactoryCommand($robot);
        $cmd="TR";
        $f2= $f->factory($cmd);
        $f2->execute();
        //assertAttributeEquals($expected, $actualAttributeName, $actualClassOrObject, $message = ''
        $this->assertAttributeEquals(
            RobotMyQ\Robot::DIRECTION_EAST
                , 'orientation'
                , $robot->getItem()
                , "Robot has to fancing east after turn right from north"
        );
    }

}
