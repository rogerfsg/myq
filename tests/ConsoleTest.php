<?php
declare(strict_types=1);
namespace RobotMyQTests;

use RobotMyQTests\TestsConstants;

use PHPUnit\Framework\TestCase;
use RobotMyQ\Console;
use RobotMyQ\Constants;

class ConsoleTest extends TestCase
{

    const FILE_OUT_RESULT_1="out_test1_result.json";

    public function testExecuteUsingFileTest1()
    {
        $console = new Console(3, array("", TestsConstants::FILE_TEST_1, self::FILE_OUT_RESULT_1));

        $this->assertTrue(
            $console->execute()
            , "Problem running test1");

        $this->assertEquals(
            TestsConstants::FILE_TEST_1
            , $console->getInputFile()
            , "Input file has to be test1.json"
        );

        $this->assertEquals(
            self::FILE_OUT_RESULT_1
            , $console->getOutputFile()
            , "Input file has to be test1.json"
        );

        $this->assertTrue(
            file_exists(self::FILE_OUT_RESULT_1)
            , "Output file not exists"
        );

        $out= file_get_contents($console->getOutputFile());
        $this->assertJson($out, "Invalid json output: $out");

        if(file_exists(self::FILE_OUT_RESULT_1))
        unlink(self::FILE_OUT_RESULT_1);

    }
}
