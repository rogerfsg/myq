<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use RobotMyQ\TrackingService;

class TrackingServiceTest extends TestCase
{
    const FILE_TEST_1="test1.json";

    /*
     *
     */
    public function testRecordVisited()
    {
        $i = 4;
        $j = 1;
        $ts = new TrackingService();
        $ts->recordVisit($i,$j);

        $this->assertEquals(array([$i, $j]), $ts->getVisits(), "Expecteds [4, 1]");
    }

    public function testRecordCleaned()
    {
        $i = 4;
        $j = 1;
        $ts = new TrackingService();
        $ts->recordClean($i,$j);

        $this->assertEquals(array([$i, $j]), $ts->getCleans(), "Expecteds [4, 1]");
    }
}