<?php
declare(strict_types=1);
namespace RobotMyQTests;

use RobotMyQTests\TestsConstants;

use PHPUnit\Framework\TestCase;
use RobotMyQ\Map;
use RobotMyQ\Robot;
use RobotMyQ\HitStrategy;
use RobotMyQ\ItemMap;
final class HitStrategyTest extends TestCase{

    private $strategy;

    protected function setUp()
    {
        $this->map= new Map();
        $this->map->init(new ItemMap(6,6, Map::STATE_FLOOR_DIRT));

        $this->robot = new Robot($this->map);
        $this->strategy = new HitStrategy($this->robot);
    }
    /*
     * Testing back command
     *         1. Turn right, then advance. (TR, A)
     */
    public function testTurnRightAndAdvanceOfStep1()
    {
        $this->map->setState(0, 1, Map::STATE_FLOOR_CANT_BE_OCUPPIED);
        //init objects
        $this->robot->start(new \RobotMyQ\ItemRobot(0, 0, Robot::DIRECTION_NORTH, 100));


        //going to the block on the right of initial position
        $result= $this->strategy->step1();

        //Map::STATE_FLOOR_WALL || $result === Map::STATE_FLOOR_CANT_BE_OCUPPIED
        $this->assertEquals(
            \RobotMyQ\RobotActionState::RESULT_HIT
            , $result
            , "No hit!");

        $strPos=(string)$this->robot->getPosition();
        $this->assertEquals("0, 0", $strPos, 'Has to be on same initial position [0,0]');
    }


    /*
 * 2. If that also hits an obstacle: Turn Left, Back, Turn Right, Advance (TL, B, TR, A)
 *
 *
 *
 *      C         C      S
 *                ^
 *                |
 *      C       Robot    S
 *
 *
 *      C         C      C
 *
 */
    public function testHitAndObstacleAndTurnLeftAndBackAndTurnRightAndAdvanceAndHit()
    {
        $this->map->init(new ItemMap(3,3, Map::STATE_FLOOR_CANT_BE_OCUPPIED));

        //preparing environment
        $this->map->setState(1, 1, Map::STATE_FLOOR_DIRT);
        $this->map->setState(1, 2, Map::STATE_FLOOR_DIRT);
        $this->map->setState(2, 2, Map::STATE_FLOOR_DIRT);

        //init robot
        $this->robot->start(new \RobotMyQ\ItemRobot( 1, 1, Robot::DIRECTION_NORTH, 100));
        $state=$this->robot->advance();
        $this->assertEquals(
            \RobotMyQ\RobotActionState::RESULT_HIT
            , $state
            , "No hit!"
        );

        //step2
        $state = $this->strategy->step2();

        //Map::STATE_FLOOR_WALL || $result === Map::STATE_FLOOR_CANT_BE_OCUPPIED
        $this->assertEquals(
            \RobotMyQ\RobotActionState::RESULT_HIT
            , $state
            , "Not Hit!");

        $strPos=(string)$this->robot->getPosition();
        $this->assertEquals("1, 2", $strPos, 'Has to be on same initial position [1, 2]');
    }


    /*
   * 3. If that also hits an obstacle: Turn Left, Turn Left, Advance (TL, TL, A)
   *
   *
   *
   *      S         S      S
   *
   *
   *     C        Robot  ->  null
   *
   *
   *      S         S      S
   *
   */
    public function testStep3()
    {
        //preparing environment
        $this->map->setState(1, 0, Map::STATE_FLOOR_CANT_BE_OCUPPIED);
        $this->map->setState(1, 2, Map::STATE_FLOOR_WALL);

        //init robot
        $this->robot->start(new \RobotMyQ\ItemRobot( 1, 1, Robot::DIRECTION_EAST, 100));
        $state= $this->robot->advance();

        $this->assertEquals(
            \RobotMyQ\RobotActionState::RESULT_HIT
            , $state
            , "No hit!"
        );

        //turn left two times
        $result= $this->strategy->step3();
        //Map::STATE_FLOOR_WALL || $result === Map::STATE_FLOOR_CANT_BE_OCUPPIED

        $this->assertEquals(
            $result
            , \RobotMyQ\RobotActionState::RESULT_HIT
            , "No hit");

        $strPos=(string)$this->robot->getPosition();
        $this->assertEquals("1, 1", $strPos, 'Has to be on same initial position [1,1]');
    }

/*
 *      4. If that also hits and obstacle: Turn Right, Back, Turn Right, Advance (TR, B, TR, A)
   *
   *
   *      S         S      C
   *
   *
   *      C        Robot  ->  null
   *
   *
   *      C         C      C
 */
    public function testStep4()
    {
        //preparing environment
        $this->map->setState(2, 2, Map::STATE_FLOOR_CANT_BE_OCUPPIED);
        $this->map->setState(1, 0, Map::STATE_FLOOR_CANT_BE_OCUPPIED);
        $this->map->setState(1, 2, Map::STATE_FLOOR_WALL);
        $this->map->setState(0, 1, Map::STATE_FLOOR_CANT_BE_OCUPPIED);
        $this->map->setState(0, 2, Map::STATE_FLOOR_CANT_BE_OCUPPIED);
        $this->map->setState(0, 3, Map::STATE_FLOOR_CANT_BE_OCUPPIED);

        //init robot
        $this->robot->start(new \RobotMyQ\ItemRobot(1, 1, Robot::DIRECTION_EAST, 100));

        //turn left two times
        $state= $this->strategy->step4();
        //Map::STATE_FLOOR_WALL || $result === Map::STATE_FLOOR_CANT_BE_OCUPPIED
        $this->assertTrue(
            $this->robot->stateCanBeOcuppied($state)
            , "The movement have to be happened");

        $strPos=(string)$this->robot->getPosition();
        $this->assertEquals("1, 1", $strPos, 'Has to be on same initial position [1, 1]');
    }

    /*
     *      5. If that also hits and obstacle: Turn Left, Turn Left, Advance (TL, TL, A)
       *
       *
       *      C         C      C
       *
       *
       *      S        Robot  ->  null
       *
       *
       *      C         C      C
     */
    public function testStep5()
    {
        //preparing environment
        $this->map->setState(0, 1, Map::STATE_FLOOR_CANT_BE_OCUPPIED);
        $this->map->setState(0, 2, Map::STATE_FLOOR_CANT_BE_OCUPPIED);
        $this->map->setState(0, 3, Map::STATE_FLOOR_CANT_BE_OCUPPIED);
        $this->map->setState(1, 2, Map::STATE_FLOOR_WALL);
        $this->map->setState(2, 0, Map::STATE_FLOOR_CANT_BE_OCUPPIED);
        $this->map->setState(2, 1, Map::STATE_FLOOR_CANT_BE_OCUPPIED);
        $this->map->setState(2, 2, Map::STATE_FLOOR_CANT_BE_OCUPPIED);

        //init robot
        $this->robot->start(new \RobotMyQ\ItemRobot(1, 1, Robot::DIRECTION_EAST, 100));

        //turn left two times
        $state= $this->strategy->step5();
        //Map::STATE_FLOOR_WALL || $result === Map::STATE_FLOOR_CANT_BE_OCUPPIED
        $this->assertTrue(
            $this->robot->stateCanBeOcuppied($state)
            , "The movement have to be happened");

        $strPos=(string)$this->robot->getPosition();
        $this->assertEquals("1, 0", $strPos, 'Has to be on same initial position [1, 0]');
    }

    /*
        *      6. If an obstacle is hit again the robot will stop and return.
      *
      *
      *      C         C      C
      *
      *
      *      S        Robot  ->  null
      *
      *
      *      C         C      C
    */
//    public function testStep6()
//    {
//
//    }

//    public function testAlgorithm()
//    {
//preparing environment
//        $this->map->setState(0, 1, Map::STATE_FLOOR_CANT_BE_OCUPPIED);
//        $this->map->setState(0, 2, Map::STATE_FLOOR_CANT_BE_OCUPPIED);
//        $this->map->setState(0, 3, Map::STATE_FLOOR_CANT_BE_OCUPPIED);
//        $this->map->setState(1, 2, Map::STATE_FLOOR_WALL);
//        $this->map->setState(2, 0, Map::STATE_FLOOR_CANT_BE_OCUPPIED);
//        $this->map->setState(2, 1, Map::STATE_FLOOR_CANT_BE_OCUPPIED);
//        $this->map->setState(2, 2, Map::STATE_FLOOR_CANT_BE_OCUPPIED);
//
//        //init robot
//        $this->robot->start(1, 1, Robot::DIRECTION_EAST);
//
//        //turn left two times
//        $state= $this->strategy->step1();
//        $this->assertFalse(
//            $this->robot->stateCanBeOcuppied($state)
//            , "The movement have to be happened");
//
//        $state= $this->strategy->step2();
//        $this->assertFalse(
//            $this->robot->stateCanBeOcuppied($state)
//            , "The movement have to be happened");
//
//        $state= $this->strategy->step3();
//        $this->assertFalse(
//            $this->robot->stateCanBeOcuppied($state)
//            , "The movement have to be happened");
//
//        $state= $this->strategy->step4();
//        $this->assertFalse(
//            $this->robot->stateCanBeOcuppied($state)
//            , "The movement have to be happened");
//
//        $state= $this->strategy->step5();
//        $this->assertFalse(
//            $this->robot->stateCanBeOcuppied($state)
//            , "The movement have to be happened");
//
//        $strPos=(string)$this->robot->getPosition();
//        $this->assertEquals("1, 0", $strPos, 'Has to be on same initial position [1, 0]');
//    }

}