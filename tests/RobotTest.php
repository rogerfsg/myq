<?php
declare(strict_types=1);
namespace RobotMyQTests;

use RobotMyQTests\TestsConstants;

use PHPUnit\Framework\TestCase;
use RobotMyQ\Map;
use RobotMyQ\Robot;
use RobotMyQ\ItemMap;
use RobotMyQ\ItemRobot;

class RobotTest extends TestCase
{

    private $map, $robot;

    protected function setUp()
    {
        $this->map= new Map();
        $this->map->init(new ItemMap( 6,6, Map::STATE_FLOOR_DIRT));
        $this->robot = new Robot($this->map);
    }
    /*
     * Start a map with just one cell and dirt. And it has to be cleaned until the end
     */
    public function testRobotCleaning(){


        $this->robot->start(new ItemRobot( 0,0, Robot::DIRECTION_SOUTH, 100));
        $this->assertTrue($this->robot->isFloorStateDirt() , "First place is dirt");
        $this->robot->cleanFloor();

        $this->assertContains(array(0, 0), $this->robot->getCleans(), "[0,0] continues dirt!");

    }

    /*
     * Test the command of advance to the North
     */
    public function testAdvanceToNorth(){
        $this->robot->start(new ItemRobot(0,0, Robot::DIRECTION_SOUTH, 100));
        $this->robot->advance();
        $strPosition= (string)$this->robot->getPosition();
        $this->assertEquals("1, 0", $strPosition , "Moving to the block above yours.");
    }

    /*
     * Test the command left+advance
     */
    public function testGoToLeftWhenGoingToNorthThenComeBackToInitialPosition(){
        //init objects
        $this->robot->start(new ItemRobot(3, 1,Robot::DIRECTION_NORTH, 100));

        //going to left
        $this->robot->turnLeft();
        $this->robot->advance();
        $strPos=(string)$this->robot->getPosition();
        $this->assertEquals("3, 0", $strPos,  "Has to go the block at left");

        //moving back
        $this->robot->turnLeft();
        $this->robot->turnLeft();
        $this->robot->advance();
        $strPos=(string)$this->robot->getPosition();
        $this->assertEquals("3, 1", $strPos,  "Has to come back to initial position");

    }

    /*
     * Test the command right+advance
     */
    public function testGoToRightWhenGoingToNorthThenComeBackToInitialPosition(){
        //init objects
        $this->robot->start(new ItemRobot(3, 0, Robot::DIRECTION_NORTH, 100));

        //going to the block on the right of initial position
        $this->robot->turnRight();
        $this->assertAttributeEquals(
            Robot::DIRECTION_EAST,
            'orientation',
            $this->robot->getItem(),
            "Has to be pointing to east");
        $this->robot->advance();
        $strPos=(string)$this->robot->getPosition();
        $this->assertEquals("3, 1", $strPos, 'Has to be on the block in next right of [3,0]');

        //moving down
        $this->robot->turnRight();
        $this->robot->advance();
        $strPos=(string)$this->robot->getPosition();
        $this->assertEquals("4, 1", $strPos , "Has to be one row down");
        $this->assertAttributeEquals(
            Robot::DIRECTION_SOUTH,
            'orientation',
            $this->robot->getItem(),
            "Has to be pointing to south");
    }

/*
 * Testing back command
 *         1. Turn right, then advance. (TR, A)
 */
    public function testTurnRightAndAdvance()
    {
        $this->map->setState(0, 1, Map::STATE_FLOOR_CANT_BE_OCUPPIED);
        //init objects
        $this->robot->start(new ItemRobot( 0, 0, Robot::DIRECTION_NORTH, 100));


        //going to the block on the right of initial position
        $this->robot->turnRight();
        $this->assertAttributeEquals(
            Robot::DIRECTION_EAST,
            'orientation',
            $this->robot->getItem(),
            "Has to be pointing to east");
        $result= $this->robot->advance();
        //Map::STATE_FLOOR_WALL || $result === Map::STATE_FLOOR_CANT_BE_OCUPPIED
        $this->assertEquals(
            \RobotMyQ\RobotActionState::RESULT_HIT
            , $result
            , "Cannot do the movement or because has exists a wall or because it cant be ocuppeid");

        $strPos=(string)$this->robot->getPosition();
        $this->assertEquals("0, 0", $strPos, 'Has to be on same initial position [0,0]');
    }

/*
 * 2. If that also hits an obstacle: Turn Left, Back, Turn Right, Advance (TL, B, TR, A)
 *
 *
 *
 *      S         C      S
 *                ^
 *                |
 *     null    Robot    S
 *
 *
 *      S         S      S
 *
 */
    public function testHitAndObstacleAndTurnLeftAndBackAndTurnRightAndAdvanceAndHit()
    {
        //preparing environment
        $this->map->setState(1, 0, Map::STATE_FLOOR_WALL);
        $this->map->setState(2, 1, Map::STATE_FLOOR_CANT_BE_OCUPPIED);

        //init robot
         $this->robot->start(new ItemRobot( 1, 1, Robot::DIRECTION_NORTH, 100));
        $this->assertNotEquals(
            \RobotMyQ\RobotActionState::RESULT_HIT
            , $this->robot->advance()
            ,"Hit!"
            );

        //going to the block on the right of initial position
        $this->robot->turnLeft();
        $this->assertAttributeEquals(
            Robot::DIRECTION_WEST,
            'orientation',
            $this->robot->getItem(),
            "Has to be pointing to west");
        $this->robot->back();
        $this->assertEquals("0, 2", (string)$this->robot->getPosition(), "After back the robot has to be in [1, 2]");
        $this->robot->turnRight();
        $state= $this->robot->advance();
        //Map::STATE_FLOOR_WALL || $result === Map::STATE_FLOOR_CANT_BE_OCUPPIED
        $this->assertTrue(
            $this->robot->stateCanBeOcuppied($state)
            , "Cannot happen a hit in [2, 2]");

        $strPos=(string)$this->robot->getPosition();
        $this->assertEquals("0, 2", $strPos, 'Has to be on same initial position [0, 2]');
    }

    /*
     * 3. If that also hits an obstacle: Turn Left, Turn Left, Advance (TL, TL, A)
     *
     *
     *
     *      S         S      S
     *
     *
     *     C        Robot  ->  null
     *
     *
     *      S         S      S
     *
     */
    public function testHitAndTurnLeftAndTurnLeftAndAdvanceAndHit()
    {
        //preparing environment
        $this->map->setState(1, 0, Map::STATE_FLOOR_CANT_BE_OCUPPIED);
        $this->map->setState(1, 2, Map::STATE_FLOOR_WALL);

        //init robot
        $this->robot->start(new ItemRobot(1, 1, Robot::DIRECTION_EAST, 100));
        $this->assertEquals(
            \RobotMyQ\RobotActionState::RESULT_HIT
            , $this->robot->advance()
            , "No hit!"
        );

        //turn left two times
        $this->robot->turnLeft();
        $this->robot->turnLeft();
        $this->assertAttributeEquals(
            Robot::DIRECTION_WEST,
            'orientation',
            $this->robot->getItem(),
            "Has to be pointing to west after double left turn");
        $result= $this->robot->advance();
        //Map::STATE_FLOOR_WALL || $result === Map::STATE_FLOOR_CANT_BE_OCUPPIED
        $this->assertEquals(
            \RobotMyQ\RobotActionState::RESULT_HIT
            , $result
            , "Cannot do the movement or because has exists a wall or because it cant be ocuppeid");

        $strPos=(string)$this->robot->getPosition();
        $this->assertEquals("1, 1", $strPos, 'Has to be on same initial position [1,1]');
    }


    /*
     * Test to read initial position
     */
    public function testReadInitialPosition()
    {
        //preparing environment
        $this->map->setState(1, 0, Map::STATE_FLOOR_CANT_BE_OCUPPIED);
        $this->map->setState(1, 2, Map::STATE_FLOOR_WALL);

        //init robot
        $this->robot->start(new ItemRobot(1, 1, Robot::DIRECTION_EAST, 100));
        $result =$this->robot->advance();

        $this->assertEquals(
            \RobotMyQ\RobotActionState::RESULT_HIT
            , $result
            ,  "No hit!" );

        //turn left two times
        $this->robot->turnLeft();
        $this->robot->turnLeft();
        $this->assertAttributeEquals(
            Robot::DIRECTION_WEST,
            'orientation',
            $this->robot->getItem(),
            "Has to be pointing to west after double left turn");
        $result= $this->robot->advance();

        $this->assertEquals(
            \RobotMyQ\RobotActionState::RESULT_HIT
            , $result
            , "No hit");

        $strPos=(string)$this->robot->getPosition();
        $this->assertEquals("1, 1", $strPos, 'Has to be on same initial position [1,1]');
    }


}