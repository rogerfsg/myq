<?php
declare(strict_types=1);
namespace RobotMyQTests;

use RobotMyQTests\TestsConstants;

use PHPUnit\Framework\TestCase;
use RobotMyQ\Context;
use RobotMyQ\RobotFacade;
use RobotMyQ\Robot;
use RobotMyQ\FactoryContextFromFile;
use RobotMyQ\JsonOutputStream;
use SebastianBergmann\CodeCoverage\Report\Xml\Tests;

class RobotFacadeTest extends TestCase
{

    public function testSystemRunningOfTestFile1()
    {
        $factory = new FactoryContextFromFile(TestsConstants::FILE_TEST_1);
        $context = $factory->factory();
        $facade = new RobotFacade($context );
        $facade->runRobotInMap();
        //Visited has be baased in test1_result.json
        $visited=$context->getVisited();
        $this->assertAttributeEquals(
            54
            , 'battery'
            , $context->getRobot()->getItem()
            , 'Battery has to be on 54'
        );

        $this->assertEquals(
            "0, 2"
            , (string)$context->getRobot()->getPosition()
            , 'Position has to be [0, 2] of file test 1'
        );

        $this->assertAttributeEquals(
            Robot::DIRECTION_EAST
            , 'orientation'
            , $context->getRobot()->getItem()
            , 'Orientation has to be E of file test 1'
        );
    }

    public function testSystemRunningOfTestFile2()
    {
        $factory = new FactoryContextFromFile(TestsConstants::FILE_TEST_2);
        $context = $factory->factory();
        $facade = new RobotFacade($context );
        $facade->runRobotInMap();
        $outputStream = new JsonOutputStream($context->getRobot());
        $json= $outputStream->print();
        $jsonInput = file_get_contents(TestsConstants::FILE_RESULT_TEST_2);
        $obj2 = json_decode($jsonInput);
        $jsonInput2 = json_encode($obj2);
        $this->assertEquals($jsonInput2, $json);
    }

}
