<?php
declare(strict_types=1);
namespace RobotMyQTests;

use RobotMyQTests\TestsConstants;

use PHPUnit\Framework\TestCase;
use RobotMyQ\FactoryContextFromFile;
use RobotMyQ\Map;
use RobotMyQ\Robot;
use RobotMyQ\Context;
use RobotMyQ\CommandBack;
use RobotMyQ\ItemMap;
use RobotMyQ\ItemRobot;
use RobotMyQ\Constants;

class CommandManagerTest extends TestCase
{

    /*
     * Will execute every command provinient form test 1
     */
    public function testFactoryCommandManagerOfTest1()
    {
        $factory = new FactoryContextFromFile(TestsConstants::FILE_TEST_1);
        $context = $factory->factory();
        $commandManager= $context->getCommandManager();
        $stack= [ "TL","A","C","A","C","TR","A","C"];

        return $this->assertEquals(
            $commandManager->getQueue()
                , $stack
                , "All executed commands has to be equal from the predefined stack");
    }


    public function testRedoAndExecuteFirstCommandsOfTest1()
    {
        $map= new Map();
        $map->init(new ItemMap(1,1, Map::STATE_FLOOR_DIRT));

        $factory = new FactoryContextFromFile(TestsConstants::FILE_TEST_1);
        $context = $factory->factory();

        $commandManager= $context->getCommandManager();
        //[ "TL","A","C","A","C","TR","A","C"];
        $robot = $commandManager->getFactoryCommand()->getRobot();
        //assertAttributeEquals($expected, $actualAttributeName, $actualClassOrObject,

        $this->assertAttributeEquals(
            Robot::DIRECTION_NORTH
            ,    'orientation'
            , $robot->getItem()
            , "The robot has to be pointing to North");
        //TL
        $result = $commandManager->redo();
        $this->assertEquals(
            $result
            , \RobotMyQ\RobotActionState::RESULT_EXECUTED
            , "Command executed");

        $this->assertAttributeEquals(
            Robot::DIRECTION_WEST
            ,    'orientation'
            , $robot->getItem()
            , "The robot has to be pointing to West"
        ) ;

    }

    public function testCommandBackItsProhibited(){

        $map= new Map();
        $map->init(new ItemMap(1,1, Map::STATE_FLOOR_DIRT));

        $factory = new \RobotMyQ\FactoryContextManually(
            $map
            , new ItemRobot(0,0,Robot::DIRECTION_EAST, 100)
            , array(CommandBack::ID)
        );
        $context= $factory->factory();
        $commandManager= $context->getCommandManager();
        $this->assertFalse(
            $commandManager->hasNext()
            , "Queue has to be empty because back command it's not allowed");

    }
}