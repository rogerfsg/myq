<?php
declare(strict_types=1);
namespace RobotMyQTests;

use RobotMyQTests\TestsConstants;

use PHPUnit\Framework\TestCase;
use RobotMyQ\Context;
use RobotMyQ\Robot;
use RobotMyQ\ItemRobot;
use RobotMyQ\ItemMap;
use RobotMyQ\Map;

class RobotWalkStrategyTest extends TestCase
{


    /*
     *
     * Turn right, then advance. (TR, A)
     *  C       C       C
     *  C       Robot-> C
     *  C       S       C
     */
    public function testWalkStep1WhenHitSomething()
    {
        $map = new Map();
        $map->init(new ItemMap(3, 3, Map::STATE_FLOOR_CANT_BE_OCUPPIED));
        $map->setState(1,1, Map::STATE_FLOOR_DIRT);
        $map->setState(0,1, Map::STATE_FLOOR_DIRT);
        $factory = new \RobotMyQ\FactoryContextManually(
            $map
            , new ItemRobot(1,1,Robot::DIRECTION_EAST, 100)
            , array("A")
        );
        $context = $factory->factory();

        $strategy = $context->getRobotWalkStrategy();
        $state = $strategy->run();
        $this->assertEquals(
            \RobotMyQ\RobotActionState::RESULT_EXECUTED
                , $state
                , "State was diferent than executed");
        $this->assertEquals(
             "0, 1"
            , (string) $context->getRobot()->getPosition()
            , "Position has to be (0, 1)"
        );
    }

    public function testValidateOrientationChangingOfTest1()
    {
        $factory = new \RobotMyQ\FactoryContextFromFile(TestsConstants::FILE_TEST_1);
        $context= $factory->factory();
        $robot = $context->getRobot();
        $cm= $context->getCommandManager();
//        Battery starts on 80
//        "TL","A","C","A","C","TR","A","C"
        //TL
        $cm->redo();
        $this->assertOrientation($robot, Robot::DIRECTION_WEST);
        $cm->redo();
        $cm->redo();
        $cm->redo();
        $cm->redo();
        $cm->redo();
        //After TR
        $this->assertOrientation($robot, Robot::DIRECTION_NORTH);

    }

    public function testRouteOfTest1()
    {
        $factory = new \RobotMyQ\FactoryContextFromFile(TestsConstants::FILE_TEST_1);
        $context= $factory->factory();
        $robot = $context->getRobot();
        $cm= $context->getCommandManager();
//        Battery starts on 80
//        "TL","A","C","A","C","TR","A","C"
        //TL
        $cm->redo();
        //A
        $cm->redo();
        $this->assertEquals(
            "0, 2"
            , $robot->getPosition()
        );
        //C
        $cm->redo();
        //A
        $cm->redo();
        $this->assertEquals(
            "0, 1"
            , $robot->getPosition()
        );
        //C
        $cm->redo();
        //TR -now facing north
        $cm->redo();
        //After TR
        $this->assertOrientation($robot, Robot::DIRECTION_NORTH);
        $state= $cm->redo();
        //After A - it will hit because it's facing north
        $this->assertHit($state );

        //So it will turn right
        $hitStrategy = new \RobotMyQ\HitStrategy($robot);
        //TR + A - will face E and go Forward
        $hitStrategy->step1();
        $this->assertOrientation($robot, Robot::DIRECTION_EAST);
        $this->assertEquals(
            "0, 2"
            , $robot->getPosition()
        );
    }

    public function testBatteryOfTest1()
    {
        $factory = new \RobotMyQ\FactoryContextFromFile(TestsConstants::FILE_TEST_1);
        $context= $factory->factory();
        $robot = $context->getRobot();
        $cm= $context->getCommandManager();
//        Battery starts on 80
//        "TL","A","C","A","C","TR","A","C"
        //TL
        $cm->redo();
        $this->assertAttributeEquals(
            79
            , 'battery'
            , $robot->getItem()
        );

        //A
        $cm->redo();
        $this->assertAttributeEquals(
            77
            , 'battery'
            , $robot->getItem()
        );
        //C
        $cm->redo();
        $this->assertAttributeEquals(
            72
            , 'battery'
            , $robot->getItem()
        );
        //"A","C","TR","A","C"
        //A
        $cm->redo();
        $this->assertAttributeEquals(
            70
            , 'battery'
            , $robot->getItem()
        );
        //C
        $cm->redo();
        $this->assertAttributeEquals(
            65
            , 'battery'
            , $robot->getItem()
        );
        //TR -now facing north
        $cm->redo();
        $this->assertAttributeEquals(
            64
            , 'battery'
            , $robot->getItem()
        );
        //A
        $state= $cm->redo();
        $this->assertHit($state );
        $this->assertAttributeEquals(
            62
            , 'battery'
            , $robot->getItem()
        );
        $hitStrategy = new \RobotMyQ\HitStrategy($robot);
        $state=$hitStrategy->step1();
        $this->assertAttributeEquals(
            59
            , 'battery'
            , $robot->getItem()
        );
        $this->assertEquals(
            \RobotMyQ\RobotActionState::RESULT_EXECUTED
                , $state
                , "Hit after step1 of backoff strategy.");
        $cm->redo();
        //C
        $this->assertAttributeEquals(
            54
            , 'battery'
            , $robot->getItem()
        );

    }

    /*
     * The robot will execute each command in order until no more commands are left, a battery low condition is hit,
     * or all the back off strategies fail.
     *
     * Base on "all the back off strategies fail" - then the robot must stop
     */
    public function testBackoffStrategyFailSoRobotStop()
    {
        $map= new Map();
        $map->init(new ItemMap(1,1, Map::STATE_FLOOR_DIRT));

        $factory = new \RobotMyQ\FactoryContextManually(
            $map
            , new ItemRobot(0,0,Robot::DIRECTION_EAST, 100)
            , array("A")
        );
        $context = $factory->factory();
        $strategy = $context->getRobotWalkStrategy();
        $state = $strategy->run();

        $this->assertHit($state);
        $this->assertPosition($context->getRobot(), "0, 0", "The map has just one cell, so it always will fail");
    }


    private function assertHit($state){
        $this->assertEquals(\RobotMyQ\RobotActionState::RESULT_HIT, $state, "No hit!" );
    }

    private function assertExecuted($state){
        $this->assertEquals(\RobotMyQ\RobotActionState::RESULT_EXECUTED, $state, "No executed!" );
    }

    private function assertPosition(Robot $robot, string $strPosition)
    {
        $this->assertEquals(
            $strPosition
            , $robot->getPosition()
        );
    }

    private function assertOrientation($robot, $direction)
    {
        $this->assertAttributeEquals(
            $direction
            , 'orientation'
            , $robot->getItem()
        );
    }
}