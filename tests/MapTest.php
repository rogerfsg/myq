<?php
declare(strict_types=1);
namespace RobotMyQTests;

use RobotMyQTests\TestsConstants;

use PHPUnit\Framework\TestCase;
use RobotMyQ\Map;
use RobotMyQ\FactoryContextFromFile;
use RobotMyQ\Context;

final class MapTest extends TestCase
{


    /*
     * Based on the file received inside test
     *
     * Input:
     * {
          "map": [
            ["S", "S", "S", "S"],
            ["S", "S", "C", "S"],
            ["S", "S", "S", "S"],
            ["S", "null", "S", "S"]
          ],
    */
    public function testMapOfTest1()
    {
        $factory = new FactoryContextFromFile(TestsConstants::FILE_TEST_1);
        $map = $factory->factoryMap();

        $this->assertEquals(count($map->getMap()) ,  4, "File test 1 has a map with 4 rows");

        $this->assertEquals(count($map->getMap()) , 4, "File test 1 has a map with 4 columns");

        //Comparing the matrix read from the file it's equal to be expected from the test1 file
        $this->assertEquals($map->getMap() , [
                ["S", "S", "S", "S"],
                ["S", "S", "C", "S"],
                ["S", "S", "S", "S"],
                ["S", "null", "S", "S"]
            ], "Map has the this predefined state matrix");
        return ;
    }

    public function testColumnOnCell21OfTest1HasToBeAColumn()
    {
        $factory = new FactoryContextFromFile(TestsConstants::FILE_TEST_1);
        $map = $factory->factoryMap();
        $this->assertEquals(
            Map::STATE_FLOOR_CANT_BE_OCUPPIED
            , $map->getState(1,2)
            , "Does not contains a cloumn on [2, 1]");

    }


    public function testInitialPositionOfTest1()
    {
        $factory = new FactoryContextFromFile(TestsConstants::FILE_TEST_1);
        $context= $factory->factory();
        $this->assertEquals(
            "0, 3"
            , (string)$context->getRobot()->getPosition()
            , "Does not contains a cloumn on [2, 1]");
    }

}