<?php
declare(strict_types=1);
namespace RobotMyQTests;

use RobotMyQTests\TestsConstants;
use PHPUnit\Framework\TestCase;
use RobotMyQ\Api;


class ApiTest extends TestCase
{

    public function testSimulateRobotOnMapOfTest1()
    {
        $api = new Api();
        $retorno=$api->simulateRobotOnMap(TestsConstants::FILE_TEST_1);
        self::assertJson($retorno);

    }
}
