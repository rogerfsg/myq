<?php
declare(strict_types=1);
namespace RobotMyQTests;

use RobotMyQTests\TestsConstants;

use PHPUnit\Framework\TestCase;
use RobotMyQ\Context;
use RobotMyQ\Robot;
use RobotMyQ\FactoryContextFromFile;
use RobotMyQ\RobotFacade;
use RobotMyQ\JsonOutputStream;

class JsonOutputStreamTest extends TestCase
{



    public function testResultWithOutputStream()
    {
        $factory = new FactoryContextFromFile(TestsConstants::FILE_TEST_1);
        $context = $factory->factory();
        $facade = new RobotFacade($context );
        $facade->runRobotInMap();

        $outputStream = new JsonOutputStream($context->getRobot());
        $json= $outputStream->print();
        $jsonInput = file_get_contents(TestsConstants::FILE_RESULT_TEST_1);
        $obj2 = json_decode($jsonInput);
        $jsonInput2 = json_encode($obj2);
        $this->assertEquals($jsonInput2, $json);
    }


}