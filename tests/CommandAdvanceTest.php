<?php
declare(strict_types=1);
namespace RobotMyQTests;

use RobotMyQTests\TestsConstants;

use PHPUnit\Framework\TestCase;
use RobotMyQ\FactoryCommand;
use RobotMyQ\Context;
use RobotMyQ;
use RobotMyQ\FactoryContextFromFile;
use RobotMyQ\Constants;

/**
 * Class CommandAdvanceTest - Command Pattern - execute the advance robot's command.
 */
class CommandAdvanceTest extends TestCase
{

    /*
     * Will execute every command provinient form test 1
     */
    public function testAdvance()
    {
        $factory = new FactoryContextFromFile(TestsConstants::FILE_TEST_1);
        $context = $factory->factory();
        $robot= $context->getRobot();
        $robot->start( new \RobotMyQ\ItemRobot(0,0, RobotMyQ\Robot::DIRECTION_SOUTH, 100));

        $f = new FactoryCommand($robot);
        $cmd="A";
        $f2= $f->factory($cmd);
        $state = $f2->execute();
        //assertAttributeEquals($expected, $actualAttributeName, $actualClassOrObject, $message = ''
        $this->assertEquals(
            "1, 0"
                , (string)$robot->getPosition()
                , "Robot has to advance!"
        );

        $this->assertEquals(\RobotMyQ\RobotActionState::RESULT_EXECUTED, $state);
    }

}
