<?php
declare(strict_types=1);
namespace RobotMyQTests;

use PHPUnit\Runner\Exception;
use RobotMyQTests\TestsConstants;
use PHPUnit\Framework\TestCase;
use RobotMyQ\Upload;
use RobotMyQ\Constants;
use GuzzleHttp;

class UploadTest extends TestCase
{

    public function testSendFileToWebservice()
    {
        try{
            $uploadFile = new Upload();
            $constants = new Constants();
            $ret =$uploadFile->sendFile(
                $constants->URL_API()
                , TestsConstants::FILE_TEST_1
                , Constants::MAX_ALLOWED_TIME_IN_SECONDS
            );
            $this->assertJson(
                $ret
                , "Upload not ok! Return: $ret");
        }catch(GuzzleHttp\Exception\RequestException $ex){

            $this->fail($ex->getMessage());
        }

    }

}