<?php
declare(strict_types=1);
namespace RobotMyQTests;

use RobotMyQTests\TestsConstants;

use PHPUnit\Framework\TestCase;

use RobotMyQ\Context;
use RobotMyQ\Robot;
use RobotMyQ\FactoryCommand;
use RobotMyQ\FactoryContextFromFile;

class CommandCleanTest extends TestCase
{


    public function testCleanFloorOfTestFile1()
    {
        $factory = new FactoryContextFromFile(TestsConstants::FILE_TEST_1);
        $context = $factory->factory();
        $robot= $context->getRobot();
        $robot->start(new \RobotMyQ\ItemRobot( 0,0, Robot::DIRECTION_SOUTH, 100));
        $this->assertTrue($robot->isFloorStateDirt() , "First place is dirt");
        $f = new FactoryCommand($robot);
        $cmd="C";
        $f2= $f->factory($cmd);
        $f2->execute();
        //assertAttributeEquals($expected, $actualAttributeName, $actualClassOrObject, $message = ''
        $this->assertTrue($robot->isFloorStateClean(), "Continued dirt!");
    }

}
