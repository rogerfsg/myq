<?php
declare(strict_types=1);
namespace RobotMyQTests;

use RobotMyQTests\TestsConstants;

use PHPUnit\Framework\TestCase;
use RobotMyQ\Context;
use RobotMyQ\FactoryContextFromFile;
use RobotMyQ\Constants;

class ContextTest extends TestCase
{
    /*
     * Will construct context off the app
     */
    public function testFactoryOfContext()
    {
        $factory = new FactoryContextFromFile(TestsConstants::FILE_TEST_1);
        $context = $factory->factory();
        $map= $context->getMap();
        $robot = $context->getRobot();
        $commandManager= $context->getCommandManager();

        $this->assertEquals(
            $robot->getMap()
            , $map
            , "Both maps has to had the same reference");
        $this->assertEquals(
            $robot
            , $commandManager->getFactoryCommand()->getRobot()
            , "Both robots has to had the same reference");
    }

}
