<?php
declare(strict_types=1);
namespace RobotMyQTests;

use RobotMyQTests\TestsConstants;

use PHPUnit\Framework\TestCase;
use RobotMyQ\Download;
use RobotMyQ\Constants;

class DownloadTest extends TestCase
{
    public function testUploadFileWithoutSentAFile()
    {
        $upload = new Download();
        $this->assertFalse($upload->receiveFile(), "No file was sent!");
    }
}
