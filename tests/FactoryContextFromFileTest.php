<?php
declare(strict_types=1);
namespace RobotMyQTests;

use RobotMyQTests\TestsConstants;

use PHPUnit\Framework\TestCase;
use RobotMyQ\FactoryContextFromFile;
use RobotMyQ\Robot;
use RobotMyQ\Constants;

class FactoryContextFromFileTest extends TestCase
{

    private $factory;
    protected function setUp()
    {
        $this->factory = new FactoryContextFromFile(TestsConstants::FILE_TEST_1);
    }

    /*
     * Factorying robot
     */
    public function testFactoryRobotOfFileTest1(){

        $robot = $this->factory->factoryRobot($this->factory->factoryMap());
        $this->assertEquals( "0, 3", (string)$robot->getPosition(),  "Position different from expected in file");

        $this->assertAttributeEquals(
            Robot::DIRECTION_NORTH,
            'orientation',
            $robot->getItem(),
            "Robot has to be oriented to N"
            );

        $this->assertAttributeEquals(
            80,
            'battery',
            $robot->getItem(),
            "Baterry has to be on 80"
        );
        return ;
    }

    /*
     * Factorying commands
     */
    public function testFactoryCommandManagerOfFileTest1(){

        $commandManager = $this->factory->factoryCommandManager(
            $this->factory->factoryRobot(
                $this->factory->factoryMap()
            )
        );
        $q = $commandManager->getQueue();
        $this->assertEquals($q,[ "TL","A","C","A","C","TR","A","C"], "List of commands has to be equal");

        return ;
    }

    public function testFactoryMapOfFileTest1()
    {
        $map = $this->factory->factoryMap();
        $this->assertTrue(count($map->getMap()) == 4, "File test 1 has a map with 4 rows");

        $this->assertTrue(count($map->getMap()) == 4, "File test 1 has a map with 4 columns");

        //Comparing the matrix read from the file it's equal to be expected from the test1 file
        $this->assertEquals($map->getMap() , [
                ["S", "S", "S", "S"],
                ["S", "S", "C", "S"],
                ["S", "S", "S", "S"],
                ["S", "null", "S", "S"]
            ], "Map has the this predefined state matrix");
        return ;
    }


}