<?php
require(__DIR__.'/../vendor/autoload.php');

try{
    $robotController = new \RobotMyQ\RobotController();
    $msg = $robotController->actionSimulate();
}catch(Exception $ue){

    $protocol = new \RobotMyQ\Protocol();
    $protocol->fail($ue->getMessage());
    return;
}
