<?php
declare(strict_types=1);
namespace RobotMyQ;

use PHPUnit\Runner\Exception;
use RobotMyQ\Robot;
use RobotMyQ\Strategy;

/**
 * Represents the backoff strategy aproach. Used when the robot hits a column, wall or an obstacle.
 */
class HitStrategy implements Strategy
{
    private $robot;

    /**
     * @param $robot Robot that will execute the HitStrategy on map
     */
    public function __construct(Robot $robot)
    {
        $this->robot=$robot;
    }

    /**
     * 1. Turn right, then advance. (TR, A)
     */
    public function step1()
    {
        return $this->executeActions(array('turnRight', 'advance'));
    }

    /**
     * 2. If that also hits an obstacle: Turn Left, Back, Turn Right, Advance (TL, B, TR, A)
     */
    public function step2()
    {
        return $this->executeActions(array('turnLeft', 'back', 'turnRight', 'advance'));
    }

    /**
     *3. If that also hits an obstacle: Turn Left, Turn Left, Advance (TL, TL, A)
     */
    public function step3()
    {
        return $this->executeActions(array('turnLeft', 'turnLeft', 'advance'));
    }

    /**
     * 4. If that also hits and obstacle: Turn Right, Back, Turn Right, Advance (TR, B, TR, A)
     */
    public function step4()
    {
        return $this->executeActions(array('turnRight', 'back', 'turnRight', 'advance'));
    }

    /**
     * 5. If that also hits and obstacle: Turn Left, Turn Left, Advance (TL, TL, A)
     */
    public function step5()
    {
        return $this->executeActions(array('turnLeft', 'turnLeft', 'advance'));
    }

    private function executeActions(array $actions)
    {
        $state = RobotActionState::RESULT_EXECUTED;
        foreach($actions as $action){
            $state= $this->robot->$action();
            if($state === RobotActionState::RESULT_OUT_OF_BATTERY)
                return $state;
        }
        return $state;
    }
    /**
     * The robot will execute each command in order until no more commands are left,
     *  a battery low condition is hit, or all the back off strategies fail.
     */
    public function run()
    {
        $functions = array('step1', 'step2', 'step3', 'step4', 'step5');
        $state =RobotActionState::RESULT_HIT;
        foreach ($functions as $f){
            $state = $this->$f();
            if(RobotActionState::isStateOutOfBatteryOrExecuted($state))
                return $state;
        }
        return $state ;
    }

}
