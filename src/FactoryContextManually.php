<?php
declare(strict_types=1);
namespace RobotMyQ;

use RobotMyQ\Robot;
use RobotMyQ\Map;

use PHPUnit\Runner\Exception;

/**
 * User can determine a Map in input
 */
class FactoryContextManually implements FactoryContext
{
    private $map, $itemRobot, $commands;
    public function __construct(
        Map $map
        , ItemRobot $itemRobot
        ,array $commands = null)
    {
        $this->map=$map;
        $this->itemRobot=$itemRobot;
        $this->commands=$commands == null ? array() : $commands;
    }

    public function factoryRobot(Map $map){
        $robot = new Robot($map);
        if(!$robot->start($this->itemRobot))
            return null;
        return $robot;
    }

    public function factoryCommandManager(Robot $robot){

        return new CommandManager(
            $this->commands
            , $robot);
    }

    public function factoryMap(){


        return $this->map;
    }

    /**
     * @return Context
     */
    public function factory()
    {
        $map= $this->factoryMap();
        $robot= $this->factoryRobot($map);
        return new Context(
            $map
            , $robot
            ,$this->factoryCommandManager($robot));
    }
}
