<?php
declare(strict_types=1);
namespace RobotMyQ;

use PHPUnit\Runner\Exception;
use RobotMyQ\OutputStream;
use RobotMyQ\Robot;

/**
 * Responsible for format the result of the robot walking in map
 */
class JsonOutputStream implements OutputStream
{
    private $robot;
    public function __construct(Robot $robot)
    {
        $this->robot=$robot;
    }

    /**
     * Return a json containing the result report
     */
    public function print()
    {
        $obj = new \stdClass();
        $obj->visited = $this->formatArray($this->robot->getVisits());
        $obj->cleaned = $this->formatArray($this->robot->getCleans() );
        $p= $this->robot->getPosition();
        $obj->final= new \stdClass();
        $obj->final->X = $p->X();
        $obj->final->Y = $p->Y();
        $obj->final->facing =$this->robot->getItem()->orientation;
        $obj->battery = $this->robot->getItem()->battery;
        return json_encode($obj);
    }

    private function removeDuplicityVisits(&$array)
    {
        for($i = 0; $i < count($array); $i++){
            for($j = $i + 1; $j < count($array); $j++)
            {
                if($array[$i] === $array[$j]){
                    array_splice($array, $j, 1);
                }
            }
        }
    }

    private function formatArray($array){
        $this->removeDuplicityVisits($array);
        usort($array, array('RobotMyQ\JsonOutputStream', 'sortByXY'));
        return $this->factoryCleanXY($array);
    }
    static function sortByXY($a, $b) {
        if($a[1] < $b[1]) return -1;
        else if($a[1] === $b[1]){
            if($a[0] === $b[0]) return 0;
            else return $a[0] < $b[0] ? -1 : 1;
        }
        else return 1;
    }


    private function factoryCleanXY($array){
        $ret = array();
        foreach($array as $c){
            $obj = new \stdClass();
            $obj->X = $c[1];
            $obj->Y = $c[0];
            array_push( $ret,$obj);
        }
        return $ret;
    }
}