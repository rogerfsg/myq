<?php
declare(strict_types=1);
namespace RobotMyQ;

use RobotMyQ\Strategy;

/**
 * Responsible to provide a strategy of robot's walking based on a queue of commands provided by CommandManager
 */
class RobotWalkStrategy implements Strategy
{
    private $robot;
    private $hitStrategy;
    private $commandManager;
    public function __construct(Robot $robot, CommandManager $commandManager)
    {
        $this->robot=$robot;
        $this->hitStrategy=new HitStrategy($robot);
        $this->commandManager = $commandManager;
    }
    public function run()
    {
        $lastState = RobotActionState::RESULT_HIT;
        while($this->commandManager->hasNext()
            && $lastState !== RobotActionState::RESULT_OUT_OF_BATTERY)
        {
            $lastState = $this->commandManager->redo();
            if($lastState === RobotActionState::RESULT_HIT) {
                $lastState = $this->hitStrategy->run();
                //if backoff strategy fails the robot stop
                if($lastState === RobotActionState::RESULT_HIT)
                    break;
            }
        }
        return $lastState;
    }
}