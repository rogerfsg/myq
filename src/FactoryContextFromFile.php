<?php
declare(strict_types=1);
namespace RobotMyQ;


use RobotMyQ\Robot;
use RobotMyQ\ItemRobot;
use RobotMyQ\DeveloperException;
use PHPUnit\Runner\Exception;

/**
 * Provide a input json file that contains the information about map, robot, and all context problem information
 * {
    "map": [
    ["S", "S", "S", "S"],
    ["S", "S", "C", "S"],
    ["S", "S", "S", "S"],
    ["S", null, "S", "S"]
    ],
    "start": {"X": 3, "Y": 0, "facing": "N"},
    "commands": [ "TL","A","C","A","C","TR","A","C"],
    "battery": 80
    }
 */
class FactoryContextFromFile implements FactoryContext
{
    private $item;

    public function __construct($pathFile)
    {
        if(!file_exists($pathFile)) throw new DeveloperException("File not exists: $pathFile");
        $this->item=json_decode(file_get_contents($pathFile));
    }

    public function factoryRobot(Map $map){
        $robot = new Robot($map);
        $robot->start(
            new ItemRobot( $this->item->start->Y
            , $this->item->start->X
            , $this->item->start->facing
            , $this->item->battery));
        return $robot;
    }

    public function factoryCommandManager(Robot $robot){

        return new CommandManager(
            $this->item->commands
            , $robot);
    }

    public function factoryMap(){
        $map = new Map($this->item->map);

        return $map;
    }

    /**
     * @return Context
     */
    public function factory()
    {
        $robot= $this->factoryRobot($this->factoryMap());
        return new Context(
            $this->factoryMap()
            , $robot
            ,$this->factoryCommandManager($robot));
    }
}
