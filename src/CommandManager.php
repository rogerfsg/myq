<?php
declare(strict_types=1);
namespace RobotMyQ;

use PHPUnit\Runner\Exception;
use RobotMyQ\DeveloperException;

/**
 * Controls a queue of robot commands
 *
 * Command Pattern
 */
class CommandManager
{
    private $queue;
    private $factoryCommand;

    const BLACK_LIST = array(CommandBack::ID);

    public function __construct(array $queue, Robot $robot )
    {
        $this->setQueue($queue);
        $this->factoryCommand = new FactoryCommand($robot);
    }

    private function setQueue($queue)
    {
        $this->queue=array();
        while(!empty($queue)){
            $item = array_shift($queue);
            if(array_search($item, CommandManager::BLACK_LIST) === false)
                array_push( $this->queue, $item);
        }
    }

    public function getQueue(){ return $this->queue; }

    public function hasNext() { return !empty($this->queue); }

    /**
     * Execute the first item in redo queue
     */
    public function redo()  {
        $cmd = array_shift( $this->queue);
        $obj = $this->factoryCommand->factory($cmd);
        if($obj == null)
            throw new DeveloperException(
                "Command {$cmd} not developped"
            );
        return $obj->execute();
    }

    public function getFactoryCommand() { return $this->factoryCommand; }

    public function redoAll()
    {
        while($this->hasNext())
        {
            $this->redo();
        }
    }
}