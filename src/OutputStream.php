<?php

declare(strict_types=1);
namespace RobotMyQ;

use PHPUnit\Runner\Exception;
use RobotMyQ\ItemMap;
interface OutputStream
{

    public function print();
}