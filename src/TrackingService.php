<?php
declare(strict_types=1);
namespace RobotMyQ;

/**
 * Responsible to register all information during the robot's cleaning
 */
class TrackingService
{
    private $visits= array();
    private $cleans = array();
    /**
     * Push the visited floor in history
     */
    public function recordVisit($i, $j)
    {
        array_push($this->visits, [$i, $j]);
    }
    /**
     * Return the stack of visits happened, with possible duplications
     */
    public function getVisits(){
        return $this->visits;

    }
    /**
     * Push the floor cleaned in history
     */
    public function recordClean($i, $j)
    {
        array_push($this->cleans, array($i, $j));
    }
    /**
     * Return the stack of cleaning happened, with possible duplications
     */
    public function getCleans()
    {
        return $this->cleans;
    }

    /**
     * Check if the coordinate corresponds to a clean floor
     */
    public function isClean($i, $j)
    {
        return array_search([$i, $j], $this->cleans) !== false;
    }
}
