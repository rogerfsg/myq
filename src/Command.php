<?php
declare(strict_types=1);
namespace RobotMyQ;

/**
 * Command Pattern
 */
interface Command
{
    public function execute();

}
