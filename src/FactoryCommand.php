<?php
declare(strict_types=1);
namespace RobotMyQ;

use PHPUnit\Runner\Exception;
use RobotMyQ\CommandAdvance;

/**
 * Responsible to create all instance of hierarchy commands
 *
 * Factory Pattern
 */
class FactoryCommand
{
    private $robot;
    public function __construct(Robot $robot)
    {
        $this->robot=$robot;
    }

    public function factory(string $cmd)
    {
        switch ($cmd)
        {
            case CommandTurnLeft::ID:
                return new CommandTurnLeft($this->robot);
            case CommandTurnRight::ID:
                return new CommandTurnRight($this->robot);
            case CommandAdvance::ID:
                return new CommandAdvance($this->robot);
            case CommandClean::ID:
                return new CommandClean($this->robot);
            case CommandBack::ID:
                return new CommandBack($this->robot);
        }
    }

    public function getRobot() {return $this->robot;}
}
