<?php
declare(strict_types=1);
namespace RobotMyQ;

use RobotMyQ;

/**
 * Responsible to receive the commands from application layer
 */
class RobotController
{
    /**
     * Do the download of file received
     */
    private function downloadFile()
    {
        $upload = new RobotMyQ\Download( );
        $upload->checkFileThrowsException();

        $arq = $upload->receiveFile();
        if($arq === false){
            throw new RobotMyQ\DeveloperException($upload->getLastError());
        }

        return $arq;
    }

    /**
     * Download file of the input provided to console application
     */
    public function actionSimulate()
    {

        $arq = null;
        try{
            $arq = $this->downloadFile();
            $protocol = new RobotMyQ\Protocol();
            if(!isset($_FILES['file_name'])){
                $protocol->fail("Empty file");
                return;
            }

            $api = new RobotMyQ\Api();

            $msg = $api->simulateRobotOnMap(Constants::TMP_PATH.$arq);

            if(isset($msg) && !empty($msg)){
                $protocol->json($msg);
            } else {
                $protocol->fail("Failed simulating rote - return null");
                return;
            }
        }finally
        {
            //Delete the received file
            if($arq !== null)
                unlink(Constants::TMP_PATH.$arq);
        }
    }
}
