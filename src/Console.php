<?php
declare(strict_types=1);
namespace RobotMyQ;

use RobotMyQ\UserException;
use RobotMyQ\Context;

use RobotMyQ\FactoryContextFromFile;
use PHPUnit\Runner\Exception;

/***
 * Responsible to control the user input
 */
class Console
{
    private $inputFile, $outputFile, $constants;


    public function __construct($argc, $argv)
    {
        if(php_sapi_name() != 'cli'){
            throw new UserException("Only can be called by console");
        }
        if($argc !== 3){
            throw new UserException(
                "Command not recognize. Option: php Console.php [json input file] [json output file]"
            );
        }
        $this->inputFile = $argv[1];
        if(!file_exists($this->inputFile)) {
            throw new UserException(
                "Input file not exists: {$this->inputFile}"
            );
        }

        $this->outputFile= $argv[2];
        if(file_exists($this->outputFile)) {
            unlink($this->outputFile);
        }

        $this->constants = new Constants();



    }

    /***
     * Function called when ENTER it's hit
     */
    public function execute()
    {
        try{
            $sendFile = new Upload();
            $ret =$sendFile->sendFile(
                $this->constants->URL_API()
                , $this->inputFile
                , Constants::MAX_ALLOWED_TIME_IN_SECONDS
            );
            $jsonObj = json_decode($ret);

            if(!isset($ret) || $jsonObj === null) {
                echo "Error during process. Try again. If persists contact our team.";
                return false;
            }
            else {
                if(file_put_contents($this->outputFile, $ret) > 0){
                    echo "Ok! Output file: {$this->outputFile}";
                    return true;
                }else{
                    echo "Error saving file: ".$this->outputFile;
                    return false;
                }
            }
        }catch(Exception $ex)
        {
            echo "Fatal erro: ".$ex->getMessage().". Contact our team.";
            return false;
        }

    }

    /**
     * DEPRECATED
     *
     * execute all the program in console side
     */
    public function executeLocal()
    {
        $factoryContext = new FactoryContextFromFile($this->inputFile);
        $context =$factoryContext->factory();
        $robotFacade = new  RobotFacade($context);
        $robotFacade->runRobotInMap();
        return $robotFacade->saveResultReportInFileAsJson($this->outputFile);
    }

    /**
     * Return the name of input file
     */
    public function getInputFile() { return $this->inputFile; }

    /**
     * Return the name of output file
     */
    public function getOutputFile() { return $this->outputFile; }
}
