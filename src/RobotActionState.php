<?php

declare(strict_types=1);
namespace RobotMyQ;

use PHPUnit\Runner\Exception;

/**
 * Contains all possible states to be used in return of Commands execution
 *
 * Pattern state
 */
class RobotActionState
{
    /**
        * Result of robot operation
        */
    const RESULT_EXECUTED = "E";
    const RESULT_OUT_OF_BATTERY = "O";
    const RESULT_HIT = "H";

    /**
     * Return if the current state is
     *  RESULT_OUT_OF_BATTERY or RESULT_EXECUTED
     */
    public static function isStateOutOfBatteryOrExecuted($state)
    {
        return ($state === RobotActionState::RESULT_OUT_OF_BATTERY
        || $state === RobotActionState::RESULT_EXECUTED);
    }

}