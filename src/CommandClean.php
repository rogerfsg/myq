<?php
declare(strict_types=1);
namespace RobotMyQ;

use RobotMyQ\Command;

class CommandClean implements Command
{
    const ID = "C";
    public $robot;

    public function __construct(Robot $robot)
    {
        $this->robot=$robot;
    }

    public function execute()
    {
        return $this->robot->cleanFloor();
    }

    public function __toString()
    {
        return self::ID;
    }
}
