<?php
declare(strict_types=1);
namespace RobotMyQ;

use RobotMyQ\Map;
use RobotMyQ\Robot;
use RobotMyQ\CommandManager;

class Context
{
    private $robot, $map, $commandManager, $walkStrategy;

    public function __construct(Map $map, Robot $robot, CommandManager $commandManager)
    {
        $this->robot=$robot;
        $this->map=$map;
        $this->commandManager=$commandManager;
        $this->walkStrategy=new RobotWalkStrategy($robot, $commandManager);
    }


    public function getMap() {return $this->map;}
    public function getRobot() {return $this->robot;}
    public function getCommandManager() {return $this->commandManager;}
    public function getVisited(){ return $this->robot->getVisits(); }

    public function getRobotWalkStrategy(){return $this->walkStrategy;}

    public function getCleaned(){ return $this->robot->getCleans(); }
}