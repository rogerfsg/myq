<?php
declare(strict_types=1);
namespace RobotMyQ;

use PHPUnit\Runner\Exception;

class Position
{
    private $i,$j;
    public function __construct($i, $j)
    {
        $this->i=$i;
        $this->j = $j;
    }
    public function __toString()
    {
        return "{$this->i}, {$this->j}";
    }
    public function Y(){return $this->i;}
    public function X(){return $this->j;}
}