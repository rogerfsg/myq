<?php
declare(strict_types=1);
namespace RobotMyQ;
use GuzzleHttp;

/**
 * Download a file to the client
 */
class Upload
{
    const DEFAULT_TIMEOUT_SEC = 5;
    public function sendFile($url, $file, $timeoutSecods = Upload::DEFAULT_TIMEOUT_SEC)
    {
        try {
            $client = new GuzzleHttp\Client([
                // You can set any number of default request options.
                'timeout' => $timeoutSecods,
            ]);

            // Provide an fopen resource.
            $r = $client->request('POST', $url, [
                'multipart' => [

                    [
                        'name' => 'file_name',
                        'contents' => fopen($file, 'r')
                    ]
                ]
            ]);

            if ($r->getStatusCode() === 200)
                return (string)$r->getBody();
            return false;
        }catch (\GuzzleHttp\Exception\ServerException $e) {
            throw new SupportException("Server problem during $url call", $e);
        }
    }

}
