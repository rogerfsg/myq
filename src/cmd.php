<?php

require( __DIR__.'/../vendor/autoload.php');

use RobotMyQ\Console;
use RobotMyQ\UserException;
use RobotMyQ\Constants;

try{
    $const = new Constants();
//    echo "API: ".$const->URL_API();
    $console = new RobotMyQ\Console($argc, $argv);
    $console->execute();
}catch(\RobotMyQ\UserException $ue){
    echo $ue->getMessage();
}

