<?php
declare(strict_types=1);
namespace RobotMyQ;

use Exception;

/**
 * Has a high chance to be an usability issue
 */
class UserException extends Exception
{
    // Redefine the exception so message isn't optional
    public function __construct($message, $code = 0, Exception $previous = null) {
        // some code

        // make sure everything is assigned properly
        parent::__construct($message, $code, $previous);
    }

}