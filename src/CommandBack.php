<?php
declare(strict_types=1);
namespace RobotMyQ;

use RobotMyQ\Command;

class CommandBack implements Command
{
    const ID = "B";
    public $robot;
    public $hitStrategy;
    public function __construct(Robot $robot)
    {
        $this->robot=$robot;
        $this->hitStrategy = new HitStrategy($robot);
    }

    public function execute()
    {
        return $this->robot->back();
    }

    public function __toString()
    {
        return self::ID;
    }
}
