<?php
declare(strict_types=1);
namespace RobotMyQ;

use RobotMyQ\Command;

class CommandTurnLeft implements Command
{
    const ID = "TL";
    public $robot;

    public function __construct(Robot $robot)
    {
        $this->robot=$robot;
    }

    public function execute()
    {
        return $this->robot->turnLeft();
    }

    public function __toString()
    {
        return self::ID;
    }
}
