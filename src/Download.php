<?php
declare(strict_types=1);
namespace RobotMyQ;

use RobotMyQ;

/**
 * Download a file from $_FILE to a specific output path
 */
class Download
{

    /**
     * @var string max valus permit to be transfere in megabytes
     */
    public $maxMB;

    /**
     * @var string output path to store the received file
     */
    public $outputPath;

    /**
     * @var name of the file stored
     */
    public $file;
    /**
     * @var string stores the last error ocurred
     */
    public $erro;

    /**
     * Download constructor.
     * @param string $uploadPath path where the file will be stored
     * @param string $maxMB same format as http://php.net/manual/en/ini.core.php#ini.upload-max-filesize
     */
    public function __construct(
        $uploadPath = Constants::TMP_PATH
        , $maxMB = Constants::LIMIT_FILE_SIZE_KB)
    {
        if(!isset($_FILES['file_name']))
            return ;
        //TODO remove and put inside dockerfile
        ini_set("upload_max_filesize", $maxMB);

        $file = $_FILES['file_name'];
        $this->file = $file;
        $this->outputPath = $uploadPath;
        $this->maxMB=$maxMB;

        $this->erro=null;
    }

    /**
     * @return If an error ocurr it will return a description of the problem
     */
    public function getLastError(){ return $this->erro; }

    /**
     * Receive a file from $_FILE
     * @return false if not ok, check getLastError(). Otherwise, the filename created
     */
    public function receiveFile()
    {
        if(!isset($this->file))
            return false;
        if ($this->file["name"] === "")
        {
            $this->erro = "Empty file!";
            return false;
        }
        else
        {
            if ($this->maxMB != "")
            {
                if ($this->file["size"] > $this->maxMB)
                {
                    $this->erro = "Size exceed {$this->file["size"]} > {$this->maxMB}";
                    return false;
                }
            }

            $out = rand(1, 99999). strtotime("now") .  $this->file["name"];

            if (move_uploaded_file($this->file["tmp_name"], $this->outputPath . $out) !== false)
            {
                return $out;
            }
            else
            {
                $strFile = print_r($this->file, true);

                $this->erro = "Upload fails. Temporary file: {$this->file["tmp_name"]}: $strFile" ;

                return false;
            }
        }
    }

    /**
     * Check if the file has been sent
     */
    public function checkFileThrowsException()
    {
        if(!isset($_FILES['file_name']))
        {
            throw new RobotMyQ\DeveloperException("Missing file in POST");
        }
    }

}
