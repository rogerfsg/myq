<?php
declare(strict_types=1);
namespace RobotMyQ;

/**
 * Represent the subsystem formed by the Robot, Map, and the movements executed by the Commands controled by CommandManager
 *
 * Facade pattern
 */
class RobotFacade
{
    /**
     * Context of the robot in the map
     */
    private $context;

    public function __construct(Context $context)
    {
        $this->context=$context;
    }
    /**
     *  Will execute every command provenient from the context
     */
    public function runRobotInMap()
    {

        $strategy = $this->context->getRobotWalkStrategy();
        return $strategy->run();
    }

    /**
     * Get the report of robot activities as a json format
     *
     * @return string json history robot activities during commands execution
     */
    public function getResultReportAsJson()
    {
        $outputStream = new JsonOutputStream($this->context->getRobot());
        return $outputStream->print();
    }

    /**
     * Save the result as json in outputfile
     */
    public function saveResultReportInFileAsJson($outputFile){

        return file_put_contents($outputFile, $this->return ) !== false;
    }

}
