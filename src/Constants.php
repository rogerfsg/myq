<?php
declare(strict_types=1);
namespace RobotMyQ;

use Dotenv;
/**
 * Constants that can be changed in each environment
 *
 */
class Constants
{
    /**
     * Url correspoding to the API called by console robot app
     *
     * Eg.: "http://localhost/robotmyq/myqRest/src/index.php"
     */
    private $urlApi;
    public function URL_API() {return $this->urlApi;}
    /**
     * Timeout of console side to the HTTP call
     */
    const MAX_ALLOWED_TIME_IN_SECONDS = 5;

    /**
     * Max size allowed per file that can be send by console app
     *
     * Same format used in: http://php.net/manual/en/ini.core.php#ini.upload-max-filesize
     */
    const LIMIT_FILE_SIZE_KB= "10000";


    /**
     * Temporary files will be stored
     */
    const TMP_PATH = "../tmp/";




    public function __construct()
    {
        //load the .env file in the current directoryenv
        $dotenv = new Dotenv\Dotenv(__DIR__);

        //Priorize the already defined UrlApp, than the file
        $dotenv->load();

        $this->urlApi=getenv("ApiUrl");

    }
}
