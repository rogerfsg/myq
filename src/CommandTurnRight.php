<?php
declare(strict_types=1);
namespace RobotMyQ;

use RobotMyQ\Command;

class CommandTurnRight implements Command
{
    const ID = "TR";
    public $robot;

    public function __construct(Robot $robot)
    {
        $this->robot=$robot;
    }

    public function execute()
    {
        return $this->robot->turnRight();
    }

    public function __toString()
    {
        return self::ID;
    }
}
