<?php
declare(strict_types=1);
namespace RobotMyQ;

/**
 * Configuration to construct the map
 */
class ItemMap
{
    public $rows, $columns, $defaultState;
    public function __construct(
        int $rows
            ,int  $columns
            , string $defaultState)
    {
        $this->rows=$rows;
        $this->columns=$columns;
        $this->defaultState=$defaultState;
    }
}
