<?php
declare(strict_types=1);
namespace RobotMyQ;
use PHPUnit\Runner\Exception;
use RobotMyQ\Download;

/**
 * Represent the functionality provided by the api, responsible simulates the robot executing commands that possibility his
 * movements on the map
 */
class Api
{



    public function simulateRobotOnMap($inputFile )
    {
            $factoryContext = new FactoryContextFromFile($inputFile);
            $context =$factoryContext->factory();
            $robotFacade = new  RobotFacade($context);
            $robotFacade->runRobotInMap();
            $json= $robotFacade->getResultReportAsJson();
            return $json;

    }

}
