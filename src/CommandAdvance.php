<?php
declare(strict_types=1);
namespace RobotMyQ;

use RobotMyQ\Command;

class CommandAdvance implements Command
{
    const ID = "A";
    public $robot;

    public function __construct(Robot $robot)
    {
        $this->robot=$robot;
    }

    public function execute()
    {
        return $this->robot->advance();
    }

    public function __toString()
    {
        return self::ID;
    }
}
