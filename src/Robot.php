<?php
declare(strict_types=1);
namespace RobotMyQ;

use PHPUnit\Runner\Exception;
use RobotMyQ\Position;
use RobotMyQ\TrackingService;

/**
 * Represents the robot that can walk in Map
 */
class Robot
{
    /**
     * Battery unit costs per robot action
     */
    const COST_BATTERY_ADVANCE = 2;
    const COST_BATTERY_BACK = 3;
    const COST_BATTERY_TURN_LEFT = 1;
    const COST_BATTERY_TURN_RIGHT= 1;
    const COST_BATTERY_CLEAN= 5;

    private $item;

    private $trackingService;
    /**
         * Enumerates cardinal orientation
        *    N
        * W     E
        *    S
        */
    const DIRECTION_SOUTH = "S";
    const DIRECTION_WEST = "W";
    const DIRECTION_NORTH = "N";
    const DIRECTION_EAST = "E";


    public function __construct(Map $map )
    {
        $this->map = $map;
        $this->trackingService=new TrackingService();
    }

    /**
     * Set a initial position of robot in map
     */
    public function start(ItemRobot $item) {
        if(!$this->stateCanBeOcuppied(
            $this->map->getState($item->i, $item->j)
        ))
            return false;

        $this->item = $item;
        $this->trackingService->recordVisit($this->item->i, $this->item->j);
        return true;
    }

    /**
     * Check if the floor that the robot is, if it is clean
     */
    public function isFloorStateClean(){
        return $this->trackingService->isClean($this->item->i, $this->item->j);
    }
    /**
     * Check if the floor that the robot is, if it is dirt
     */
    public function isFloorStateDirt(){
        return !$this->isFloorStateClean($this->item->i, $this->item->j);
    }

    /**
     * Ask to robot clean the floor
     */
    public function cleanFloor(){
        if(!$this->consumeBattery(Robot::COST_BATTERY_CLEAN) )
            return RobotActionState::RESULT_OUT_OF_BATTERY;

        $this->trackingService->recordClean($this->item->i, $this->item->j);
        return RobotActionState::RESULT_EXECUTED;
    }

    /**
     *  Has to move to another block based on the current orientation
     *   [i, j]
     *          N
     *      W       E
     *          S
     */
    public function advance(){
        //Robot consumes battery even if hit an obstacle
        if(!$this->consumeBattery(Robot::COST_BATTERY_ADVANCE) )
            return RobotActionState::RESULT_OUT_OF_BATTERY;

        $i = $this->item->i;
        $j = $this->item->j;
        switch ($this->item->orientation){
            case Robot::DIRECTION_SOUTH:
                $i++;
                break;
            case Robot::DIRECTION_NORTH:
                $i--;
                break;
            case Robot::DIRECTION_EAST:
                $j++;
                break;
            case Robot::DIRECTION_WEST:
                $j--;
                break;
        }
        $state= $this->map->getState($i, $j) ;
        if($this->stateCanBeOcuppied($state))
        {
            $this->item->i = $i;
            $this->item->j = $j;

            $this->trackingService->recordVisit($i, $j);
            return RobotActionState::RESULT_EXECUTED;
        } else return RobotActionState::RESULT_HIT;
    }

    /**
    *  Has to move to another block based on the current orientation
    *   [i, j]
    *          N
    *      W       E
    *          S
    */
    public function back(){
        //Robot consumes battery even if hit an obstacle
        if(!$this->consumeBattery(Robot::COST_BATTERY_BACK) )
            return RobotActionState::RESULT_OUT_OF_BATTERY;

        $i = $this->item->i;
        $j = $this->item->j;
        switch ($this->item->orientation){
            case Robot::DIRECTION_SOUTH:
                $i--;
                break;
            case Robot::DIRECTION_NORTH:
                $i++;
                break;
            case Robot::DIRECTION_EAST:
                $j--;
                break;
            case Robot::DIRECTION_WEST:
                $j++;
                break;
        }
        $state= $this->map->getState($i, $j) ;
        if($this->stateCanBeOcuppied($state))
        {
            $this->item->i = $i;
            $this->item->j = $j;
            $this->trackingService->recordVisit($i, $j);
            return RobotActionState::RESULT_EXECUTED;
        }
        else
            return RobotActionState::RESULT_HIT;
    }

    /**
     * Returns the current robot position
     */
    public function getPosition() : Position{
        return new Position($this->item->i, $this->item->j);
    }

    /**
    *   Change the robot orientation to 90 degrees to left
    */
    public function turnLeft(){
        if(!$this->consumeBattery(Robot::COST_BATTERY_TURN_LEFT) )
            return RobotActionState::RESULT_OUT_OF_BATTERY;

        switch ($this->item->orientation){
            case Robot::DIRECTION_SOUTH:
                $this->item->orientation = Robot::DIRECTION_EAST;
                break;
            case Robot::DIRECTION_NORTH:
                $this->item->orientation = Robot::DIRECTION_WEST;
                break;
            case Robot::DIRECTION_EAST:
                $this->item->orientation = Robot::DIRECTION_NORTH;
                break;
            case Robot::DIRECTION_WEST:
                $this->item->orientation = Robot::DIRECTION_SOUTH;
                break;
        }
        return RobotActionState::RESULT_EXECUTED;
    }

    /**
    *   Change the robot orientation to 90 degrees to right
    *    N
    * W     E
    *    S
    */
    public function turnRight(){
        if(!$this->consumeBattery(Robot::COST_BATTERY_TURN_RIGHT))
            return RobotActionState::RESULT_OUT_OF_BATTERY;

        switch ($this->item->orientation){
            case Robot::DIRECTION_SOUTH:
                $this->item->orientation = Robot::DIRECTION_WEST;
                break;
            case Robot::DIRECTION_WEST:
                $this->item->orientation = Robot::DIRECTION_NORTH;
                break;
            case Robot::DIRECTION_NORTH:
                $this->item->orientation = Robot::DIRECTION_EAST;
                break;
            case Robot::DIRECTION_EAST:
                $this->item->orientation = Robot::DIRECTION_SOUTH;
                break;
        }
        return RobotActionState::RESULT_EXECUTED;
    }
    /**
     * Tells if the state correspend to a position in floor can be ocuppied
     */
    public function stateCanBeOcuppied($state){
        return $state != Map::STATE_FLOOR_CANT_BE_OCUPPIED
            && $state != Map::STATE_FLOOR_WALL;
    }
    /**
     * Will consume the battery if it has enough energy
     */
    public function consumeBattery(int $units){
        if($this->item->battery - $units <= 0)
            return false;
        $this->item->battery -=$units;
        return true;
    }

    public function getMap() {return $this->map;}

    public function getVisits(){
        return $this->trackingService->getVisits();
    }

    public function getCleans(){
        return $this->trackingService->getCleans();
    }

    public function getItem(){return $this->item;}
}
