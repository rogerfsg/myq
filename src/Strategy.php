<?php
declare(strict_types=1);
namespace RobotMyQ;

/**
 * Design patter strategy
 */
interface Strategy
{

    public function run();
}