<?php
declare(strict_types=1);
namespace RobotMyQ;

/**
 * Configuration to initialize the Robot on map
 */
class ItemRobot
{
    public $i, $j, $orientation, $battery;

    public function __construct(
        int $i
        ,int  $j
        , string $orientation
        , int $battery)
    {
        $this->i = $i;
        $this->j=$j;
        $this->orientation=$orientation;
        $this->battery=$battery;
    }
}
