<?php
declare(strict_types=1);
namespace RobotMyQ;

use PHPUnit\Runner\Exception;
use RobotMyQ\ItemMap;

/**
 * Represent the map to be explored by the robot
 */
class Map
{
    /**
     * Enumerate possible floor states
     */
    const STATE_FLOOR_DIRT = "S";
    const STATE_FLOOR_CLEAN= "X";
    const STATE_FLOOR_CANT_BE_OCUPPIED= "C";
    const STATE_FLOOR_WALL= null;

    /**
     * Complete item read from file and decoded
     */
    private $map;

    /**
     *  @param array $item corresponds to the matrix of the MAP. Each cell can be any of the following states:
     * Map::STATE_FLOOR_DIRT, Map::STATE_FLOOR_CLEAN. Map::STATE_FLOOR_CANT_BE_OCUPPIED, Map::STATE_FLOOR_WALL
     *
     */
    public function __construct(array $item = null)
    {
        $this->map=$item;
    }

    /**
     * init map with a default state
     */
    public function init(ItemMap $itemMap)
    {
        $this->map = new \stdClass();
        $this->map = array_fill(
            0
                , $itemMap->rows
                , array_fill(
                    0, $itemMap->columns, $itemMap->defaultState
            ));
    }

    /**
     * Load the map from a persistense model, in this case file
     */
    public function load(string $pathFile)
    {
        if(!file_exists($pathFile)) throw new Exception("File not exists: $pathFile");
        $this->map=json_decode(file_get_contents($pathFile));
    }

    /**
     * Return the state matrix that represents the map
     */
    public function getMap(){
        return $this->map;
    }

    /**
     * Get state of a position
     *
     * @param int $i item row
     * @param int $j item column
     */
    public function getState(int $i, int $j){

        if(!$this->exists($i, $j))
        {
            return Map::STATE_FLOOR_WALL;
        }
        else return $this->map[$i][$j];
    }

    /**
     * Set state of a position
     *
     * @param int $i item row
     * @param int $j item column
     * @param string $state: CLEAN, DIRT, COLUMN, WALL
     */
    public function setState(int $i, int $j, string $state = null){
        $this->map[$i][$j]=$state;
    }

    /**
     * Exists cell
     * */
    public function exists(int $i, int $j){
        return isset($this->map[$i])
        && isset($this->map[$i][$j]);
    }

    /**
     * Set multiple coordinates with a state
     */
    public function setStates(array $coordinates, $state)
    {
        foreach($coordinates as $c){
            $this->setState($c[0], $c[1], $state);
        }
    }
}
