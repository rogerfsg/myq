<?php
declare(strict_types=1);
namespace RobotMyQ;

/**
 * Protocol of API communicate to the clients.
 *
 * Prefered to use html status code
 */
class Protocol
{
    /**
     * In case of failuring during a call of webservice
     */
    public function fail($msg)
    {
        header('HTTP/1.1 400 Bad Request');
        echo $msg;
    }

    /**
     * Return json message from webservie
     */
    public function json($msg)
    {
        header('Content-Type: application/json');
        echo $msg;
    }
}
