<?php
declare(strict_types=1);
namespace RobotMyQ;

/**
 * Factory pattern
 */
interface FactoryContext
{

    public function factory();
}