# MyQ - Roger Filipe Sales Gusmão

Production Environment: Preparing
	
	git clone https://myq:sPFZ7wuPetSqQhERL3VB@bitbucket.org/rogerfsg/myq/src/rest app
	
	Access directory app/:
		composer install --no-dev --ignore-platform-reqs

	docker-compose -f docker-compose.production.xml build

	docker-compose -f docker-compose.production.xml up --remove-orphans -d

Production Environment: Running

	docker exec robot-console php src/cmd.php rsc/test1.json tmp/out2240.json
	
Development Environment: Preparing - Access the path to store the aplication
	
	git clone https://myq:sPFZ7wuPetSqQhERL3VB@bitbucket.org/rogerfsg/myq/src/rest app
	
	Access directory app/:
		composer install --ignore-platform-reqs

	docker-compose -f docker-compose.development.yml build

	docker-compose -f docker-compose.development.yml up --remove-orphans -d

Development Environment: Running TDD
	
	- go to the root of project - which contains the folders: src, rsc, tests, ...
	- Discover the network generated, the suffix will be 'robot_network', in my case the nem was robotmyq_robot_network.
		- Execute the command to view the list of networks, property name: 
			docker network ls
			
	- To run all tests execute:
		WIN:
			docker run -e ApiUrl=http://website/src/index.php --link robot-website-dev:website --network (network name with suffix 'robot_network')  -v (Complete path to the projects root):/app  phpunit/phpunit:6.5.3 --config tests/config.xml
			
			Example:
				docker run -e ApiUrl=http://website/src/index.php --link robot-website-dev:website --network app_robot_network  -v C:\wamp64\www\Teste1\app:/app  phpunit/phpunit:6.5.3 --config tests/config.xml
				
			Example console:
				docker exec robot-console-dev php src/cmd.php rsc/test1.json tmp/out2240.json
		LINUX:
			docker run -e ApiUrl=http://website/src/index.php --link robot-website-dev:website --network (network name with suffix 'robot_network')   -v $(pwd):/app  phpunit/phpunit:6.5.3 --config tests/config.xml


To generate PHPDoc 
	- Requirements: prepare Development Environment	
	- Go to root path or project, and execute:
	
		vendor\bin\phpdoc run -d src\ -t doc\
	
Obs.: Maybe you have to download and install graphviz to generate the documentation. 
	TODO in the future generate the documentation on php-cli image docker.
